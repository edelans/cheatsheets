How To Make Great First Impressions: The Name Game
[original source](https://www.improveyoursocialskills.com/social-skills-how-to-guides)

Last week, we talked about how you can use body language to make great first impressions. This week, let's talk about something equally important---remembering the name of the person you just met.

People like hearing their names. When you use someone's name, it helps them to feel comfortable, and remembering their name makes it much easier for you to strike up a conversation later on.

Unfortunately, if you want to use someone's name, you need to remember it, and remembering names is hard. Fortunately, remembering names is much easier with one simple trick.

I call it "Playing the name game."

To play the name game, follow these three rules.

# Rule 1: Get their name ASAP
When you first meet someone, get their name within the first few minutes of the conversation. You can either ask for it directly, or just introduce yourself. Most folks will give you their name when you give them yours. Once you get their name, repeat it immediately. If they say "My name is Bob" say "It's great to meet you, Bob" not "It's great to meet you." The early repetition ensures that the name makes it into your short term memory.

# Rule 2: Make their name memorable
Now, it's time to move the name into your long-term memory. To do that, combine the name with a memorable adjective. A memorable adjective is either an adjective that starts with the same letter as the name, or that rhymes with the name. For instance, "Cool Carl" or "Dan the Man."

Ideally, this adjective should be somewhat related to the person ("Bob from Boston" is great if Bob is actually from Boston), but it doesn't have to be. Any combination that either rhymes or starts with the same letter will do. Repeat this combination to yourself a few times to make sure it fixes in your memory (but make sure you don't say it out loud----Mustache Mike might not appreciate the title you've given him)

# Rule 3: Lock their name in your memory
Then, cement the name/adjective combination in your memory by repeating it mentally a few times during the conversation. This is exactly as simple as it sounds---just think to yourself "That is Cool Carl" a few times during the conversation. You don't have to keep up a constant repetition in your head---just remind yourself whenever you think of it.

When you follow these three rules, you'll find remembering names are much easier. Getting their name immediately allows you to focus on their name before the conversation takes off and demands more of your attention, and repeating a memorable name/adjective combination will cement their name in your mind for the long term.

Also, one bonus rule: If you forget their name, just make sure you ask again near the end of the conversation. I find that once you've had the chance to get to know someone in a conversation, remembering their name is much easier (because you have some memories to pin the name to.) So if playing the name game at the beginning of the conversation doesn't work for you, just play it at the end. Chances are, they forgot your name too, so they'll appreciate the chance to ask again.

In any case, remembering names will still take work, and you won't remember every name.  But play the name game and I guarantee that you have much greater success remembering names.
