Telling Stories in Conversation
[see original](https://www.improveyoursocialskills.com/storytelling-in-conversation)

Stories put your ability to connect into overdrive. People are wired for storytelling, and the easiest way to someone's heart is not through their stomach but through storytelling.

Unfortunately, it's easy to tell a story badly.  You've probably been subject to rambling storytellers that take forever to get to the point -- or worse, don't seem to have a point at all. On occasion, you've probably also been one of those storytellers on occasion (sorry!)

Fortunately, it's also easy to tell a story well -- if you know how. There's just three things you need to remember.

# Grab Them From The Get-Go.
The Sound Of Music was wrong -- the beginning is not a very good place to start.

If you start at the chronological beginning of your story and take the time to explain all the details and backstory, your audience will be tuned out by the time you get to the "good part."

Instead, show why the story is important before you start telling it. For instance, if you're going to tell the story about the time you found a possum in your bed, don't just start telling the story -- say something like "Ok, let me tell you about the weirdest thing I ever woke up next to." Now you've got their interest, and they'll stay tuned in while you tell the story.

It's often a good idea to use vivid words like "weird" or "funny" when introducing your story. For instance, "Let me tell about how I met my wife" is not as good as "Let me tell you about the weird way I met my wife." The audience will stay tuned in to find out what was weird.

Also, make sure that everything in your story supports the point of the story. If you're telling a story about how you found a possum in your bed, everything in your story should be leading up to the moment of possum discovery. If you're not sure if something needs to be in your story, throw it out.

# Engage The Imagination.
Storytelling is all about imagination, so invite your audience to imagine.

An easy way to do this is to invite audience participation. For instance, find a pivotal point of the story, and then ask your listeners what they would do. Ie, "I heard a noise coming from the basement -- and I was home alone. If you were me, what would you do?"

You can also invite them to imagine what happens next. For example, "I opened the door to the basement -- what do you think I found there?"

Make sure these questions are not too open-ended but also not just asking a yes or no response. For instance, if you were telling the story of Cinderella:

"Do you think my sisters enjoyed seeing me at the ball?" is not good, because it's a yes or no question.
"I walked in the ball -- what do you think happened?" is not good, because it's very broad.
"The prince walked over to me -- and what do you think he said?" is good, because it's a good middle ground of being open-ended but not too broad.
One more tip: instead of describing everything, give one vivid detail, and let the audience imagine the rest.

For instance, instead of spending several sentences describing how opulent your hotel room was, just say "It was a beautiful hotel room -- the sheets were the smoothest I've ever felt." With that detail, the audience can easily imagine the rest of the hotel room.

# Vary Your Voice
Your voice is a powerful tool for storytelling, and it's easy to use.

Don't worry about actually sounding sad, or angry, or whatever -- you're probably not an actor. Instead, just raise or lower the volume of your voice, or speed up or slow down the tempo of your voice.

A loud and/or fast voice communicates excitement/energy, while a slow/soft voice communicates seriousness.
This is a great technique to play with -- deliver stories in the mirror or during your commute and practice vocal variety. The more your practice, the more natural your vocal variety will sound.
