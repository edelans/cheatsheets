


# View existing remotes

```bash
git remote -v
```

# Change remote name from 'origin' to 'destination'

```bash
git remote rename origin destination
```

# Change your remote's URL

```bash
git remote set-url origin https://github.com/USERNAME/REPOSITORY.git
```


# Set a new remote

```bash
git remote add origin https://github.com/user/repo.git
```

# Remove a remote

```bash
git remote rm destination
```
