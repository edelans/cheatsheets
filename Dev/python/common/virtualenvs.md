
# Create new virtualenv
use virtualenvwrapper :
```
mkvirtualenv --python=/usr/bin/python3 virtualenvName
```

# Activate virtualenv
```
workon virtualenvName
```
