# string formating

```python
'{} {}'.format(1, 2)
# 1 2

'{1} {0}'.format('one', 'two')
# two one


```

much more @ https://pyformat.info/


# import modules

## custom module

Modules are Python .py files that consist of Python code. Any Python file can be referenced as a module. A Python file called `hello.py` has the module name of `hello` that can be imported into other Python files or used on the Python command line interpreter with the `import` statement.

`import module` will look for the file `module.py` in thoses directories :

- current dir
- `sys.path` (you can either append a custom dir to the path : `sys.path.append('/usr/edelans/')` of add your module directly in the directory given by your python path (`print(sys.path)`).)



## whole module, dot notation


```python
import math
print(math.pi)
```

## specific functions

```python
from math import pi
print(pi)
```

##  whole module, direct reference (no dot notation) - DISCOURAGED

To refer to items from a module within your program’s namespace, you can use the from ... import statement. When you import modules this way, you can refer to the functions by name rather than through dot notation.

```python
from math import *
print(pi)
```

imports everything defined within the module. this is **discouraged** by PEP 8.


## Aliasing Modules

You may want to change a name because you have already used the same name for something else in your program, another module you have imported also uses that name, or you may want to abbreviate a longer name that you are using a lot.

```python
import math as m
print(m.pi)
```
