

# Run mongo shell

Connect to local MongoDB Instance on Default Port

  mongo

# Explore your mongo instance in the mongo shell

Display the database you are using

  db

list the available databases

  show dbs

Switch databases

  use <database_name>

list of collections in the current database

  show collections

# Explore objects in a collection


simple query

  db.users.find( { name: "Joe" } );

Only show specific fields of the object ("projection")

  db.users.find( { }, { name: true } );

check one object structure

  db.users.findOne( { name: "Joe" } );

count objects

  db.users.find( { name: "Joe" } ).count();

sort

  db.users.find().sort( { name: 1 } );   // sort by name in ascending order
  db.users.find().sort( { name: -1 } );  // sort by name in descending order

Limit results for better performance

  db.users.find().limit(5);

Display help

  help

Further help : [Official doc](https://docs.mongodb.com/manual/reference/mongo-shell/)



# operations in queries

## operators

### comparison

$eq, $ne : equal / not equal

$gt : greater than
$gte : greater than or equal

$lt : less than
$lte : less than or equal

$in : any of the values specified in an array
$nin : none of the values specified in an array

### logical

$and
$not
$or
$nor :  logical NOR returns all documents that fail to match both clauses

### element

$exists : Matches documents that have the specified field

## examples

The following example retrieves all documents from the inventory collection where status equals either "A" or "D":

  db.inventory.find( { status: { $in: [ "A", "D" ] } } )

The following query uses the less than operator ($lt) on the field h embedded in the size field:

  db.inventory.find( { "size.h": { $lt: 15 } } )

The following example retrieves all documents in the inventory collection where the status equals "A" and qty is less than ($lt) 30:

  db.inventory.find( { status: "A", qty: { $lt: 30 } } )

retrieves all documents in the collection where the status equals "A" or qty is less than ($lt) 30:

  db.inventory.find( { $or: [ { status: "A" }, { qty: { $lt: 30 } } ] } )

In the following example, the compound query document selects all documents in the collection where the status equals "A" and either qty is less than ($lt) 30 or item starts with the character p:

  db.inventory.find( {
       status: "A",
       $or: [ { qty: { $lt: 30 } }, { item: /^p/ } ]
  } )


### querying arrays

see https://docs.mongodb.com/manual/tutorial/query-arrays/
