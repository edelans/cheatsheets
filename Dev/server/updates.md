
# ubuntu

- `apt-get update`:
just read the entries in repository - acording to existing list. Needed to check what is new.
- `apt-get upgrade`: all updates for installed packages without kernel modules. No release update.
- `apt-get dist-upgrade`: all updates for installed packages also with kernel modules. No release update.
- `apt-get` with parameter `-s`: test only, no changes performed.
