# use systemd

Systemd provides a standard process for controlling what programs run when a Linux system boots up. It also starts a journal of system activity, the network stack, a cron-style job scheduler, user logins, and many other jobs.


For instance, It will launch gunicorn at server start, and respawn it when it fails.

## Configuration
A service configuration file is provided at the root of this repo. It should be stored in `/etc/systemd/system/`.

- Create a service /your/project/dir/myFancy.service such as:

		[Unit]
		Description=My Fancy Service
    After=multi-user.target

		[Service]
		Type=simple
		ExecStart=/usr/bin/python3  /home/myFancyPython.py
    Restart=always
    RestartSec=2

		[Install]
		WantedBy=multi-user.target


More complicated [Service] section for Django app :

		[Service]
		User=deploy
		Group=www-data
		WorkingDirectory=/var/www/trellowatch
		Environment=PATH=/home/deploy/.virtualenvs/trellowatch/bin
		Environment=DJANGO_SETTINGS_MODULE=trellowatch.settings.production
		ExecStart=/home/deploy/.virtualenvs/trellowatch/bin/gunicorn --workers 3 --bind 127.0.0.1:8000 trellowatch.wsgi:application
		Restart=always

- :warning: systemd service strips all environment variables but TERM, PATH and LANG. If you are executing the script directly nothing removes the environment variables so everything works. But via systemd, nothing works... So you have to tell the service to load the env variables. There are 3 ways to do this :

1. Reference environment variables directly in individual unit files (Environment=)
1 .Put the variable assignments in a file (like /etc/environment) and reference that file per-unit (EnvironmentFile=)
1. Put the variable assignments in the DefaultEnvironment variable in /etc/systemd/system.conf or a snippet in /etc/systemd/system.conf.d/[some file].

- for detailed options, see manual (https://www.freedesktop.org/software/systemd/man/systemd.service.html)

- Create a symbolic link between your script and a special location under /etc: (be careful : make the User from the [Service] section own the service file !)

		ln -s /your/project/dir/myFancy.service /etc/systemd/system/myFancy.service

- Make systemd aware of your new service

		sudo systemctl daemon-reload
		sudo systemctl enable myFancy.service
		sudo systemctl start myFancy.service

- If you wish to control the service at runtime, you can use systemctl commands:

		sudo systemctl status myFancy.service
		sudo systemctl stop myFancy.service
		sudo systemctl start myFancy.service
		sudo systemctl disable myFancy.service


## Logs consultation
Make use of `journalctl`. It will show the logs of gunicorn, which receive the logs Django sends to the console (depending on you logger config).

You can filter by :

- time : `journalctl --since 09:00 --until "1 hour ago"`, `journalctl --since "2015-01-10" --until "2015-01-11 03:00"`, `journalctl --since yesterday --until today`

- by service : `journalctl -u nginx.service`, journalctl -u trellowatch.service

- as you go (like `tail -f`) : `journalctl -f`

## Logs cleaning

- to keep only 1G of logs : `sudo journalctl --vacuum-size=1G`
- to keep entries from the last year : `sudo journalctl --vacuum-time=1years`
